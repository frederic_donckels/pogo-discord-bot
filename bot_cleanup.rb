require_relative 'pogo_discord_lib.rb'
require_relative 'config.rb'


#
# This bot removes the messages from channels purely based on their expiration time
# and the author, keeping some messages alive (for Virton raids)
#

bot = Discordrb::Bot.new token: configatron.token, client_id: configatron.bot_id

def webhook_author?(message)
  configatron.cleanup.webhook_ids.include? message.author.id
end

# Gets the actual channel objects
def extract_channels(bot, cleanup_server)
  configatron.cleanup.channel_ids.map do |id|
    bot.channel(id, cleanup_server)
  end
end

# Cleans the channel history when the bot starts
def clean_channels_history(cleanup_channels)
  cleanup_channels.each do |channel|
    initial_cleanup_loop(channel, proc { |m| webhook_author?(m) }, proc { |m| schedule_expiration(m) })
  end
end

# Prepare listening loop for automated messages to remove
bot.message(from: configatron.cleanup.webhook_ids) do |event|
  LOGGER.info "Message received in channel ##{event.message.channel.name}"
  schedule_expiration(event.message)
end

bot.ready do
  begin
    cleanup_server = bot.servers[configatron.cleanup.server_id]
    cleanup_channels = extract_channels(bot, cleanup_server)

    # TODO: make this optional
    clean_channels_history(cleanup_channels)
  rescue StandardError => e
    LOGGER.warn e.message
    LOGGER.warn e.backtrace
    bot.stop
  end
end

bot.run
