require_relative 'pogo_discord_lib.rb'
require_relative 'config_simple.rb'


#
# This bot removes the messages from the pogomap channels purely based on their expiration time
#

bot = Discordrb::Bot.new token: configatron.token, client_id: configatron.bot_id


# Gets the actual channel objects
def simple_extract_channels(cleanup_server)
  channels = []
  cleanup_server.channels().each do |x|
    channels << x if configatron.cleanup.channel_names.include? x.name
  end
  LOGGER.info("collected channels: #{channels.map { |x| x.name }}")
  channels
end

# Cleans the channel history when the bot starts
def simple_clean_channels_history(cleanup_channels)
  cleanup_channels.each do |channel|
    initial_cleanup_loop(channel, proc { |m| configatron.cleanup.author_names.include? m.author.name }, proc { |m| schedule_expiration(m) })
  end
end

# Prepare listening loop for automated messages to remove
bot.message(from: configatron.cleanup.author_names, in: configatron.cleanup.channel_names) do |event|
  LOGGER.info "Message received in channel ##{event.message.channel.name}"
  schedule_expiration(event.message)
end

bot.ready do
  begin
    cleanup_server = bot.servers[configatron.cleanup.server_id]
    cleanup_channels = simple_extract_channels(cleanup_server)
    simple_clean_channels_history(cleanup_channels)
  rescue StandardError => e
    LOGGER.warn e.message
    LOGGER.warn e.backtrace
    bot.stop
  end
end

bot.run
