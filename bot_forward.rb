require_relative 'pogo_discord_lib.rb'
require_relative 'config.rb'
require_relative 'geo_tools.rb'
require 'active_support/inflector'

#
# This bot forwards the notifications from the Pogomap Discord towards the local (Virton) Discord
#


def read_locale
  locale = JSON.parse(File.read('fr.json'))
  asciify(locale)
end

def asciify(value)
  if value.instance_of? String
    return value.parameterize()
  end
  if value.instance_of? Hash
    value.each do |key, sub_value|
      value[key] = asciify(sub_value)
    end
  end
  value
end

LOCALE = read_locale()
UNKNOWN = 'https://upload.wikimedia.org/wikipedia/commons/a/af/Question_mark.png'

def pokemon_icon(key)
  "https://monsterimages.tk/v1.5/regular/monsters/#{key}_000.png"
end

def egg_icon
  "https://monsterimages.tk/v1.5/regular/eggs/#{Regexp.last_match(2)}.png"
end

def find_pokemon(pokename)
  pokename = asciify(pokename)
  LOCALE['pokemon'].find {|_, value| value == pokename}
end

def parse_title(original_embed_title)
  # Egg
  if /(OEUF lvl|EGG level) (\d)/ =~ original_embed_title
    return egg_icon, Regexp.last_match(2), true
  end

  # Pokemon
  if /(lvl|level) (\d) (contre|against) ([^ !]*)/ =~ original_embed_title
    key, = find_pokemon(Regexp.last_match(4))
    return pokemon_icon(key), Regexp.last_match(2), false unless key.nil?
  end
  [UNKNOWN, '0', false]
end

def get_raid_webhook(config, zone, egg, level)
  webhooks = config[zone]
  webhooks["#{egg ? 'oeuf' : 'raid'}_#{level}"]
end

def get_ex_webhook(config, description)
  text = description.downcase
  config.special.each do |special, filter|
    return config.webhooks.raids.special[special] if text.include? filter
  end
  nil
end

def webhook_not_found(webhook)
  webhook.nil? || webhook.empty?
end

def choose_raid_webhook(point, description, level, egg)
  config = configatron.forward

  # First check the special EX raids
  webhook = get_ex_webhook(config, description)
  return webhook unless webhook.nil?

  # If not an EX, find the zone for the forward
  zone = find_zone(config, point)
  return config.webhooks.raids.default if zone.nil?

  # Then find the appropriate web hook
  webhook = get_raid_webhook(config.webhooks.raids, zone, egg, level)
  return webhook unless webhook_not_found(webhook)
  config.webhooks.raids[zone].default
end

def add_embed_details(builder, original_embed, original_image)
  puts original_embed.title
  builder.add_embed do |embed|
    embed.title = original_embed.title
    embed.description = original_embed.description
    embed.url = original_embed.url
    embed.image = Discordrb::Webhooks::EmbedImage.new(url: original_image.url) unless original_image.nil?
  end
end

def create_forwarded_content(builder, message, avatar_url, original_embed, original_image)
  builder.avatar_url = avatar_url
  builder.content = message.content
  builder.username = message.author.username
  add_embed_details(builder, original_embed, original_image)
end

def point_from(message)
  location = message.embeds[0].url.split('q=')[-1]
  lat, lon = location.split(',')
  [lat.to_f, lon.to_f]
end

def forward_raid(message)
  original_embed = message.embeds[0]
  original_image = original_embed.image
  avatar_url, level, egg = parse_title(original_embed.title)

  point = point_from(message)
  webhook = choose_raid_webhook(point, original_embed.description, level, egg)
  webhooks_client = Discordrb::Webhooks::Client.new(url: webhook)
  webhooks_client.execute(nil, true) do |builder|
    create_forwarded_content(builder, message, avatar_url, original_embed, original_image)
  end
end

def choose_pokemon_webhook(message)
  point = point_from(message)
  config = configatron.forward
  zone = find_zone(config, point)
  config.webhooks.pokemons.default if zone.nil?
  webhook = config.webhooks.pokemons[zone]
  return webhook unless webhook_not_found(webhook)
  config.webhooks.pokemons.default
end

def forward_pokemon(message)
  original_embed = message.embeds[0]
  pokename = message.author.username
  /^([a-zA-Z]*)/ =~ pokename
  key, = find_pokemon(Regexp.last_match(1))
  avatar_url = key.nil? ? UNKNOWN : pokemon_icon(key)

  webhook = choose_pokemon_webhook(message)

  webhooks_client = Discordrb::Webhooks::Client.new(url: webhook)
  webhooks_client.execute(nil, true) do |builder|
    create_forwarded_content(builder, message, avatar_url, original_embed, nil)
  end

end

bot = Discordrb::Bot.new token: configatron.token, client_id: configatron.bot_id

bot.message(from: %w[Raid Egg], in: %w[#virton-raids]) do |event|
  LOGGER.info "Message received from #{event.message.channel.name}"
  forward_raid(event.message)
end

bot.message(in: %w[#virton-pokemons virton-iv100]) do |event|
  LOGGER.info "Message received from #{event.message.channel.name}"
  forward_pokemon(event.message)
end

bot.ready do
  bot.game = 'Looking for raids & pokemons'
end

bot.run



