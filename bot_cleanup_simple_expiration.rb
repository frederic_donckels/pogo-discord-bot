require_relative 'pogo_discord_lib.rb'
require_relative 'config_expiration.rb'

DEFAULT_DURATION = 60 * 30
QUEST_DURATION = 60 * 60 * 24

#
# This bot removes the messages from channels purely based on their creation time
# (Team Rocket notifications, Daily Quests) and Webhook author
#


bot = Discordrb::Bot.new token: configatron.token, client_id: configatron.bot_id

def expired_webhook_author?(message)
  configatron.cleanup.elapsed.author_names.include? message.author.name
end

def expired_webhook_title?(message)
  return false if message.embeds.nil? || message.embeds[0].nil?
  embed = message.embeds[0]
  return embed.title =~ /^\*\*\d+.*%/ && embed.description.include?('Dispara')
end

# Gets the actual channel objects
def expired_extract_channels(bot, cleanup_server, channel_ids)
  channel_ids.map do |id|
    bot.channel(id, cleanup_server)
  end
end

# Cleans the channel history when the bot starts
def expired_clean_channels_history(cleanup_channels, duration)
  cleanup_channels.each do |channel|
    filter = lambda {|m| expired_webhook_author?(m) || expired_webhook_title?(m)}
    removal = lambda {|m| schedule_removal(m, duration)}
    initial_cleanup_loop(channel, filter, removal, duration)
  end
end

# Prepare listening loop for automated messages to remove
bot.message(from: configatron.cleanup.elapsed.author_names, in: configatron.cleanup.elapsed.team_rocket.channel_ids) do |event|
  LOGGER.info "Message received in channel ##{event.message.channel.name}"
  schedule_removal(event.message, DEFAULT_DURATION)
end

bot.message(in: configatron.cleanup.elapsed.pokemon.channel_ids) do |event|
  LOGGER.info "Message received in channel ##{event.message.channel.name}"
  schedule_removal(event.message, DEFAULT_DURATION) if expired_webhook_title?(event.message)
end

bot.message(from: configatron.cleanup.elapsed.author_names, in: configatron.cleanup.elapsed.quest.channel_ids) do |event|
  LOGGER.info "Message received in channel ##{event.message.channel.name}"
  schedule_removal(event.message, QUEST_DURATION)
end

bot.ready do
  begin
    cleanup_server = bot.servers[configatron.cleanup.server_id]
    team_rocket_channels = expired_extract_channels(bot, cleanup_server, configatron.cleanup.elapsed.team_rocket.channel_ids)
    quest_channels = expired_extract_channels(bot, cleanup_server, configatron.cleanup.elapsed.quest.channel_ids)
    pokemon_channels = expired_extract_channels(bot, cleanup_server, configatron.cleanup.elapsed.pokemon.channel_ids)

    expired_clean_channels_history(pokemon_channels, DEFAULT_DURATION)
    expired_clean_channels_history(team_rocket_channels, DEFAULT_DURATION)
    expired_clean_channels_history(quest_channels, QUEST_DURATION)
  rescue StandardError => e
    LOGGER.warn e.message
    LOGGER.warn e.backtrace
    bot.stop
  end
end

bot.run
