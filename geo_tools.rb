require 'rubygems'
require 'bundler/setup'
require 'configatron'
require 'log4r'
require_relative 'config.rb'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/CyclomaticComplexity
def point_in_polygon(point, polygon)
  j = polygon.length - 1
  i = 0
  odd_node_count = false
  while i < polygon.length do
    if (polygon[i][1] < point[1] && polygon[j][1] >= point[1]) || (polygon[j][1] < point[1] && polygon[i][1] >= point[1])
      if polygon[i][0] + (point[1] - polygon[i][1]) / (polygon[j][1] - polygon[i][1]) * (polygon[j][0] - polygon[i][0]) < point[0]
        odd_node_count = !odd_node_count
      end
    end
    j = i
    i += 1
  end

  odd_node_count
end

def find_zone(config, point)
  located_zone = nil
  config.zones.each do |zone, polygon|
    if point_in_polygon(point, polygon)
      located_zone = zone
      break
    end
  end
  located_zone
end
