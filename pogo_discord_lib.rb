require 'rubygems'
require 'bundler/setup'
require 'discordrb'
require 'discordrb/webhooks'
require 'configatron'
require 'json'
require 'concurrent'
require 'thread'
require 'log4r'

STDOUT.sync = true

MAX_LIFETIME = 60 * 60 * 2
MAX_HISTORY_EXPLORATION = 100
FETCH_SIZE = 100


LOGGER = Log4r::Logger.new('bot')
outputter = Log4r::Outputter.stdout
outputter.formatter = Log4r::PatternFormatter.new ({pattern: '%d %l %m'})
LOGGER.outputters << outputter

def expired?(message, max_lifetime = MAX_LIFETIME)
  # assumes the author is a webhook
  message.timestamp < (Time.now - max_lifetime)
end

# Computes the announced expiration time for a raid/egg message
def compute_expiration(description, notification_type)
  regexp = 'Egg' == notification_type ? /(\d+):(\d+):(\d+) (à|to)/ : /(?:à|at) (\d+):(\d+):(\d+)/
  return nil unless description =~ regexp
  today = Time.new
  # Because we know that no raids after early evening
  Time.new(today.year, today.month, today.day, Regexp.last_match(1), Regexp.last_match(2), Regexp.last_match(3))
end

def compute_delay(expiration)
  schedule_delay = expiration - Time.now
  schedule_delay = 10 if 0 >= schedule_delay
  schedule_delay
end

def delete_message(message)
  title = message.embeds[0].nil? ? "" : message.embeds[0].title
  LOGGER.info "Deleting message #{message.id} #{title} from ##{message.channel.name} (#{message.timestamp})"
  message.delete
end

def keep_cleaning_up?(count, history, max_history_exploration)
  !history.empty? && (max_history_exploration > count)
end

def schedule_removal(message, schedule_delay)
  LOGGER.info "Scheduled expiration of #{message.id} (##{message.channel.name}) for expiration  in #{schedule_delay.to_i} s "
  Concurrent::ScheduledTask.execute(schedule_delay) do
    delete_message(message)
  end
end

# Schedules a message for removal (based on its expiration time)
def schedule_expiration(message)
  description = message.embeds[0].description
  expiration = compute_expiration(description, message.author.name)
  return if expiration.nil?
  schedule_delay = compute_delay(expiration)
  schedule_removal(message, schedule_delay)
end

# Cleans a history segment
def clean_history(count, purged, history, filter, schedule_removal_op, max_lifetime = MAX_LIFETIME)
  last_retrieved = nil
  history.each do |message|
    count += 1
    last_retrieved = message.id
    next unless filter.call(message)
    if expired?(message, max_lifetime)
      delete_message(message)
      purged += 1
    else
      schedule_removal_op.call(message)
    end
  end
  [count, purged, last_retrieved]
end

def initial_cleanup_loop(channel, filter, schedule_removal_op, max_lifetime = MAX_LIFETIME)
  last_retrieved = nil
  LOGGER.info "Cleaning up channel ##{channel.name}"
  history = channel.history(FETCH_SIZE, last_retrieved)
  count = 0
  purged = 0
  while keep_cleaning_up?(count, history, MAX_HISTORY_EXPLORATION)
    LOGGER.debug "Retrieved #{history.size} messages from history"
    count, purged, last_retrieved = clean_history(count, purged, history, filter, schedule_removal_op, max_lifetime)
    history = channel.history(FETCH_SIZE, last_retrieved)
  end
  LOGGER.info "Nothing left to purge in ##{channel.name} (examined: #{count} / purged #{purged})"
end
